from alpha_vantage.timeseries import TimeSeries
import json
import pymongo
from time import sleep

key = "WBDS9SEMRQ0P21NH"
with open("../data/stock_missing_list.json") as f:
    stock_names = json.loads(f.read())

client = pymongo.MongoClient()
db = client['stock_temp']
sp_daily_col = db['sp_daily']
sp_daily_col.create_index([('id', pymongo.ASCENDING), ('date', pymongo.DESCENDING)], unique=True)


fail_stock_list = []

ts = TimeSeries(key=key)
for symbol in stock_names:
    sleep(13)
    try:
        data, meta_data = ts.get_daily(symbol)
        format_data = [{"id": symbol,
                        "date": k,
                        "open": v['1. open'],
                        "high": v['2. high'],
                        "low": v['3. low'],
                        "close": v['4. close'],
                        "volume": v['5. volume']} for k, v in data.items()]
        sp_daily_col.insert_many(format_data)
        print("{} Succeed".format(symbol))
    except:
        fail_stock_list.append(symbol)
        print("{} Failed".format(symbol))

with open("../temp/log.txt", 'w') as f:
    f.write(json.dumps(fail_stock_list))

