## Stock live back end

## How to run

After cloning the repo and move to the root directory

**install the requirements**

```
pip install -r requirements.txt
```

**run the server**

```
cd router
flask run
```

And the server will be running at http://localhost:5000/ to handle *GET* request in the form: http://localhost:5000/data?/stockId={request id}

## Things to pay attention to

The server query the data from the mongodb and the mongodb is deployed on the server belonging to HKUST VisLab. For group members, if you want to get access to server, please contract me for account & password. The database is valid until 03/02/2019. 

