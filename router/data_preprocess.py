
def preprocess(data):
    processed_data = {"id": "", "data": [], "date": [], "currentPrice": 0,
                      "lastDayClosePrice": 0, "changingRate": 0, "detailed": []}
    if len(data) == 0:
        return processed_data
    data = sorted(data, key=lambda d: d['date'])
    processed_data['id'] = data[-1][u'id']
    processed_data['currentPrice'] = float(data[-1][u'close'])
    processed_data['lastDayClosePrice'] = float(data[-1][u'open'])
    processed_data['changingRate'] = (processed_data['lastDayClosePrice'] - processed_data['currentPrice']) / \
                                     processed_data['lastDayClosePrice'] * 100
    processed_data['date'] = [d[u'date'] for d in data]
    processed_data['data'] = [float(d[u'close']) for d in data]
    processed_data['detailed'] = [{
        "open": float(d[u'open']),
        "close": float(d[u'close']),
        "high": float(d[u'high']),
        "low": float(d[u'low']),
        "volume": int(d[u'volume']) / 1000
    } for d in data]
    return processed_data


if __name__ == '__main__':

    from data_service import StockDataService
    service = StockDataService()
    data = service.fetch_data('A', '2018-10-23', '2019-01-19')
    processed = preprocess(data)
    print(processed['date'])