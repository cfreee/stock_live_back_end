from flask import Flask, json, request
from flask_cors import CORS
from data_service import StockDataService
from data_preprocess import preprocess

data_service = StockDataService()
app = Flask(__name__)
CORS(app)


@app.route('/stock', methods=['POST', 'GET'])
def fetch_data():
    stock_id = request.args.get('stockId')
    data = data_service.fetch_data(stock_id, '2018-10-23', '2019-01-19')
    processed = preprocess(data)
    return json.dumps(processed)