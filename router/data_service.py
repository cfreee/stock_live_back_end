from pymongo import MongoClient

client = MongoClient()
db = client['stock_temp']
sp_daily_col = db['sp_daily']

class StockDataService:

    def __init__(self, collection=sp_daily_col):
        self.col = collection

    def fetch_data(self, stock_id, begin_date, end_date):
        return list(self.col.find({"id": stock_id, "date": {"$gte": begin_date, "$lt": end_date}}))


if __name__ == '__main__':
    service = StockDataService()
    data = service.fetch_data('A', '2018-10-23', '2019-01-19')
    print(data[0])


